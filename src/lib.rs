pub trait Roundable {
    fn trunc_to(self, places: u16) -> Self;
    fn round_to(self, places: u16) -> Self;
}

impl Roundable for f32 {
    #[inline]
    fn trunc_to(self, places: u16) -> f32 {
        self.trunc() + (self.fract() * 10.00f32.powi(places as i32)).trunc() / 10.00f32.powi(places as i32)
    }

    #[inline]
    fn round_to(self, places: u16) -> f32 {
        self.trunc() + (self.fract() * 10.00f32.powi(places as i32)).round() / 10.00f32.powi(places as i32)
    }
}

impl Roundable for f64 {
    #[inline]
    fn trunc_to(self, places: u16) -> f64 {
        self.trunc() + (self.fract() * 10.00f64.powi(places as i32)).trunc() / 10.00f64.powi(places as i32)
    }

    #[inline]
    fn round_to(self, places: u16) -> f64 {
        self.trunc() + (self.fract() * 10.00f64.powi(places as i32)).round() / 10.00f64.powi(places as i32)
    }
}
